package co.getintouch.im.xmpp.jingle;

public interface OnPrimaryCandidateFound {
	public void onPrimaryCandidateFound(boolean success, JingleCandidate canditate);
}
