package co.getintouch.im.xmpp.jingle;

public interface OnTransportConnected {
	public void failed();
	public void established();
}
