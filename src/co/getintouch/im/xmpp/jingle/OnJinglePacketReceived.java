package co.getintouch.im.xmpp.jingle;

import co.getintouch.im.entities.Account;
import co.getintouch.im.xmpp.PacketReceived;
import co.getintouch.im.xmpp.jingle.stanzas.JinglePacket;

public interface OnJinglePacketReceived extends PacketReceived {
	public void onJinglePacketReceived(Account account, JinglePacket packet);
}
