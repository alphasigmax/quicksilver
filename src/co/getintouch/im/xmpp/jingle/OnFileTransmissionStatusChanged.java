package co.getintouch.im.xmpp.jingle;

public interface OnFileTransmissionStatusChanged {
	public void onFileTransmitted(JingleFile file);
	public void onFileTransferAborted();
}
