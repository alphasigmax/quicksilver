package co.getintouch.im.xmpp;

import co.getintouch.im.entities.Contact;

public interface OnContactStatusChanged {
	public void onContactStatusChanged(Contact contact, boolean online);
}
