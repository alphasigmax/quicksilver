package co.getintouch.im.xmpp;

import co.getintouch.im.entities.Account;


public interface OnBindListener {
	public void onBind(Account account);
}
