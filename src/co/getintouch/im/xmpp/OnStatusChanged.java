package co.getintouch.im.xmpp;

import co.getintouch.im.entities.Account;


public interface OnStatusChanged {
	public void onStatusChanged(Account account);
}
