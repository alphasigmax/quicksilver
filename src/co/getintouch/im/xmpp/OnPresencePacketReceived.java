package co.getintouch.im.xmpp;

import co.getintouch.im.entities.Account;
import co.getintouch.im.xmpp.stanzas.PresencePacket;

public interface OnPresencePacketReceived extends PacketReceived {
	public void onPresencePacketReceived(Account account, PresencePacket packet);
}
