package co.getintouch.im.xmpp;

import co.getintouch.im.entities.Account;
import co.getintouch.im.xmpp.stanzas.MessagePacket;

public interface OnMessagePacketReceived extends PacketReceived {
	public void onMessagePacketReceived(Account account, MessagePacket packet);
}
