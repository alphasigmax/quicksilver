package co.getintouch.im.xmpp;

import co.getintouch.im.entities.Account;
import co.getintouch.im.xmpp.stanzas.IqPacket;

public interface OnIqPacketReceived extends PacketReceived {
	public void onIqPacketReceived(Account account, IqPacket packet);
}
