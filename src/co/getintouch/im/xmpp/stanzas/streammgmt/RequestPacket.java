package co.getintouch.im.xmpp.stanzas.streammgmt;

import co.getintouch.im.xmpp.stanzas.AbstractStanza;

public class RequestPacket extends AbstractStanza {

	public RequestPacket(int smVersion) {
		super("r");
		this.setAttribute("xmlns","urn:xmpp:sm:"+smVersion);
	}

}
