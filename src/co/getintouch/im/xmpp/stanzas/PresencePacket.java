package co.getintouch.im.xmpp.stanzas;


public class PresencePacket extends AbstractStanza {
	
	public PresencePacket() {
		super("presence");
	}
}
