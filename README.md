
##Design principles
* Be as beautiful and easy to use as possible without sacrificing security or
  privacy
* Rely on existing, well established protocols (XMPP)
* Do not require a Google Account or specifically Google Cloud Messaging (GCM)
* Require as little permissions as possible

##Features
* End-to-end encryption with either OTR or openPGP
* Sending and receiving images
* Intuitive UI that follows Android Design guidelines
* Syncs with desktop client
* Conferences (with support for bookmarks)
* Address book integration
* Multiple Accounts / unified inbox
* Very low impact on battery life


###XMPP Features
Conversations works with every XMPP server out there. However XMPP is an extensible
protocol. These extensions are standardized as well in so called XEP’s.
Conversations supports a couple of those to make the overall user experience better. There is a
chance that your current XMPP server does not support these extensions.
Therefore to get the most out of Conversations you should consider either switching to an
XMPP server that does or - even better - run your own XMPP server for you and
your friends.
These XEPs are - as of now:
* XEP-0065: SOCKS5 Bytestreams - or rather mod_proxy65. Will be used to tranfer files if both parties are behind a firewall (NAT).
* XEP-0138: Stream Compression saves bandwith
* XEP-0198: Stream Management allows XMPP to survive small network outages and changes of the underlying TCP connection.
* XEP-0280: Message Carbons which automatically syncs the messages you send to
  your desktop client and thus allows you to switch seamlessly from your mobile
  client to your desktop client and back within one conversation.
* XEP-0237: Roster Versioning mainly to save bandwith on poor mobile connections



##FAQ



####How does the address book integration work?
The address bock integration was designed to protect your privacy. Conversations
neither uploads contacts from your address book to your server nor fills your
address book with unnecessary contacts from your online roster. If you manually
add a Jabber ID to your phones address book Conversations will use the name and
the profile picture of this contact. To make the process of adding Jabber IDs to
your address book easier you can click on the profile picture in the contact
details within Conversations. This will start an add to address book intent with the jabber ID
as payload. This doesn’t require Conversations to have write permissions on your
address book but also doesn’t require you to copy past Jabber ID from one app to
another.
####Where can I see the status of my contacts? How can I set a status or priority
Status are a horrible metric. Setting them manually to a proper value rarly
works because users are either lazy or just forget about them. Setting them
automatically does not provide quality results either. Keyboard or mouse
activity as indicator for example fails when the user is just looking at
something (reading an article, watching a movie). Furthermore automatic setting
of status always implies an impact on your privacy. (Are you sure you want
everybody in your contact list to know that you have been using your computer at
4am?!)

In the past status has been used to judge the likelihood of whether or not your
messages are being read. This is no longer necessary. With Chat Markers
(XEP-0333, supported by Conversations since 0.4) we have the ability to **know**
whether or not your messages are being read.
Similar things can be said for priorites. In the past priorties have been used
(By servers, not by clients!) to route your messages to one specific client.
With carbon messages (XEP-0280, supported by Conversations since 0.1) this is no
longer necessary. Using priorities to route OTR messages isn't pratical either
because they are not changeable on the fly. Metrics like last active client
(the client which sent the last message) are much better.

Unfortunatly these modern replacements for legacy XMPP features are not widely
adopted. However Conversations should be an instant messenger for the future and
instead of making Conversations compatible with the past we should work on
implementing new, improved technologies into other XMPP clients as well.

Making these status and priority optional isn't a solution either because
Conversations is trying to get rid of old behaviours and set an example for
other clients.



####You closed my feature request but I want it really really badly
Just write it yourself and send me a pull request. If I like it I will happily
merge it if I don't at least you and like minded people get to enjoy it.



###Security
####Why are there two end-to-end encryption methods and which one should I choose?
In most cases OTR should be the encryption method of choice. It works out of the box with most contacts as long as they are online.
However PGP can be in some cases (carbonated messages to multiple clients) be
more flexible.
####How do I use openPGP
Before you continue reading you should notice that the openPGP support in
Conversations is marked as experimental. This is not because it will make the app
unstable but because the fundamental concepts of PGP aren't ready for a
widespread use. The way PGP works is that you trust Key IDs instead of XMPP- or email addresses. So in theory your contact list should consist of Public-Key-IDs instead of email addresses. But of course no email or xmpp client out there implements these concepts. Plus PGP in the context of instant messaging has a couple of downsides. It is vulnerable to replay attacks, it is rather verbose, and decrypting and encrypting takes longer than OTR. It is however asynchronous and works well with carbonated messages.

To use openpgp you have to install the opensource app OpenKeychain (www.openkeychain.org) and then long press on the account in manage accounts and choose renew PGP announcement from the contextual menu.
####How does the encryption for conferences work?
For conferences the only supported encryption method is OpenPGP. (OTR does not
work with multiple participents.) Every participant has to announce their
OpenPGP key. (See answer above). If you would like to send encrypted messages to
a conference you have to make sure that you have every participant's public key
in your OpenKeychain. Right now there is no check in Conversations to ensure
that. You have to take care of that yourself. Go to the conference details and
touch every key id (The hexadecimal number below a contact). This will send you
to OpenKeychain which will assist you on adding the key.
This works best in very small conferences with contacts you are already using
OpenPGP with. This feature is regarded experimental. Conversations is the only
client that uses XEP-0027 with conferences. (The XEP neither specifically allows
nor disallows this.)
###Development

####How do I debug Conversations
If something goes wrong Conversations usually exposes very little information in
the UI. (Other than the fact that something didn't work)
However with adb (android debug bridge) you squeeze some more information out of
Conversations. These information are especially useful if you are experiencing
troubles with your connection or with file transfer.
````
adb -d logcat -v time -s xmppService
````

